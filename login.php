<?php

    require 'functions/theme-function.php';
    $errors = [];

    if($_SERVER["REQUEST_METHOD"] == 'POST'){
        if(empty($_POST["username"])){
            $errors[] = "Veuillez saisir un nom d'utilisateur";
        }

        if(empty($_POST["password"])){
            $errors[] = "Veuillez saisir un mot de passe";
        }

        if(strlen($_POST["password"])<=8){
            $errors[] = 'Veuillez saisir au moins 8 caractères';
        }

        if($_POST["username"] == 'aurelien' && $_POST["password"] == 'aurelien'){
            $_SESSION["username"] = "Aurélien";
            header("Location: mon-compte.php");
        }
    }
?>

<html>
<head>
    <?php
        include 'parts/global-stylesheets.php';
    ?>
</head>
<body class="<?php displayThemeClass();?>">

<div class="container">

    <form method="post" action="login.php">
        <div class="form-group">
            <label for="exampleInputEmail1">Username</label>
            <input type="text" name="username" class="form-control" id="username"  placeholder="Votre nom d'utilisateur !">
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Mot de passe</label>
            <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Mot de passe">
        </div>

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>

    <?php

        if(count($errors)!=0){
            echo('<h2>Erreurs de saisie</h2> <ol>');

            foreach ($errors as $error){
                echo('<li>'.$error.'</li>');
            }
            echo('</ol>');
        }

    ?>

</div>

<?php
    include "parts/global-scripts.php";
?>
</body>
</html>