<?php
    function getAllProduct(){
       return
           $produits =
               [
                   [
                       "nom"=> "Croquettes pour chats",
                       "description"=> "Croquettes super super bonnes",
                       "image"=> "https://www.equilibre-et-instinct.com/536-thickbox_default/sachet-de-500g-de-croquettes-pour-chat-adulte.jpg",
                       "note"=> 3,
                       "produit_flash"=> false
                   ],
                   [
                       "nom"=> "Croquettes pour chien",
                       "description"=> "Croquettes pas ouf !",
                       "image"=> "https://www.cdiscount.com/pdt2/1/7/4/1/700x700/zca654174/rw/mini-croquettes-boeuf-pour-chien-1-kg-frolic.jpg",
                       "note"=> 3,
                       "produit_flash"=> true
                   ]

               ];
    }

    function displayProductCard($produit, $key){

        $class = "white";

        if(array_key_exists("theme", $_COOKIE)){

            $class = $_COOKIE["theme"];
        }



       if(isset($_SESSION["panier"]) && in_array($key, $_SESSION["panier"])){
           echo('  <div class="card" style="width: 18rem;">
        <div class="card-body">
            <h5 class="card-title toto' . $class .'">'.$produit["nom"].'</h5>
            <a href="produits.php?action=remove-panier&index='.$key.'" class="card-link">Supprimer du panier</a>
        </div>
    </div>');
       } else {
           echo('  <div class="card" style="width: 18rem;">
        <div class="card-body">
            <h5 class="card-title ' . $class .'">'.$produit["nom"].'</h5>
            <a href="produits.php?action=panier&index='.$key.'" class="card-link">Ajouter au panier</a>
        </div>
    </div>');
       }

    }


?>