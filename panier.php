<?php
    require "functions/product-functions.php";

    $products = getAllProduct();

   // session_start();
  // Je reccupére tous mes produits
?>
<html>
<head>
    <?php
    include 'parts/global-stylesheets.php';
    ?>
</head>
<body>

<div class="container">
    <?php
    include 'parts/menu.php';
    ?>


    <h1>Mon super panier !</h1>

    <div class="row">
        <?php
            foreach ($products as $key => $product){
                if(in_array($key, $_SESSION["panier"])){
                    displayProductCard($product, $key);
                }
            }
        ?>
    </div>


</div>

<?php
include "parts/global-scripts.php";
?>
</body>
</html>