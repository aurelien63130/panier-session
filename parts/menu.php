<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">Navbar</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
            <li class="nav-item active">
                <a class="nav-link" href="produits.php">Les produits</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="mon-compte.php">Mon compte</a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="panier.php">Panier</a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="produits.php?theme=dark">Mode sombre</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="produits.php?theme=white">Mode clair</a>
            </li>
        </ul>
    </div>
</nav>