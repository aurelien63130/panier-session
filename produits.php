<?php
// session_start();

require "functions/product-functions.php";
require "functions/theme-function.php";

if(isset($_GET["theme"])){
    // Cookie valide 7 jours
    $expire = 3600*24*7;
    setcookie("theme", $_GET["theme"]);
}


if(isset($_GET["action"]) && isset($_GET["index"]) && $_GET["action"] == "panier"){
    $panier = null;
    if(array_key_exists("panier", $_SESSION)){
        $panier = $_SESSION["panier"];
    } else {
        $panier = [];
    }

    $panier[] = $_GET["index"];
    $_SESSION["panier"] = $panier;

}

if(isset($_SESSION["panier"]) && isset($_GET["action"]) && isset($_GET["index"]) && $_GET["action"] == "remove-panier"){
    $panier = $_SESSION["panier"];
    $element = $_GET["index"];

    unset($panier[array_search($element, $panier)]);

    $_SESSION["panier"] = $panier;

}

// A enlever si on veut un accès anonyme au site
if(empty($_SESSION) || is_null($_SESSION["username"])){
    header("Location: login.php");
}


$products = getAllProduct();


?>

<html>
<head>
    <?php
    include 'parts/global-stylesheets.php';
    ?>
</head>
<body class="<?php displayThemeClass();?>">

<div class="container">
    <?php
    include 'parts/menu.php';
    ?>


    <h1>Les produits de l'appli</h1>

    <div class="row <?php displayThemeClass();?>">
        <?php
        foreach ($products as $key=>$product){
            displayProductCard($product, $key);
        }
        ?>
    </div>


</div>

<?php
include "parts/global-scripts.php";
?>
</body>
</html>