<?php
    if(empty($_SESSION) || is_null($_SESSION["username"])){
        header("Location: login.php");
    }
?>

<html>
<head>
    <?php
    include 'parts/global-stylesheets.php';
    ?>
</head>
<body>

<div class="container">
    <?php
        include 'parts/menu.php';
    ?>
<h1>Mon compte ! </h1>

<h1>Ma variable de session</h1>

    <?php
        echo 'Bonjour '. $_SESSION["username"];
    ?>

    <a href="logout.php">
        <button class="btn btn-danger">Me déconnecter</button>
    </a>

</div>

<?php
include "parts/global-scripts.php";
?>
</body>
</html>